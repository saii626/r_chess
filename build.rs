use std::{process::Command, env};


fn main() {
    slint_build::compile("ui/appwindow.slint").unwrap();

    Command::new("make")
        .arg("-j")
        .arg("profile-build")
        .arg("ARCH=x86-64-avx2")
        .current_dir("./Stockfish/src")
        .output()
        .expect("Unable to build stockfish");

    let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let profile = env::var("PROFILE").unwrap();
    std::fs::copy("./Stockfish/src/stockfish", format!("{manifest_dir}/target/{profile}/stockfish")).unwrap();
}
