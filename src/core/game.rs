use enumflags2::{bitflags, make_bitflags, BitFlags};

use crate::piece;
use crate::core::board::{Board, Cell, CellIndex, PieceType, Color};
use crate::view_model::ViewState;

use super::board::{CellIndexIter, CellType, PieceMoveIterType};

const PEDANTIC_MOVES: bool = true;

macro_rules! validate {
    ($cond:expr, $($reason:tt)*) => {
        if PEDANTIC_MOVES || cfg!(debug_assertions) {
            if !$cond {
                eprint!("Board invariant does not hold. ");
                eprintln!($($reason)*);
                panic!()
            }
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum MoveType {
    Normal,
    Capture,

    PawnDoubleMove,
    EnPassant,
    PawnPromotion (Option<PieceType>),

    KingSideCastling,
    QueenSideCastling,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum KingPositions {
    Secure,
    Vulnerable { side: Color, king_cell: CellIndex, attack_index: CellIndex },
    Defeated { side: Color, king_cell: CellIndex }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Move {
    from: CellIndex,
    to: CellIndex,
    typ: MoveType,
    //positions: KingPositions,
}

#[derive(Debug)]
pub struct PossibleMoves(Vec<Move>);

impl Board<ViewState> {

    pub fn show_move(&mut self, to_show: &Move) {
        self[to_show.from].set_update();
        self[to_show.to].set_update();
    }

    pub fn clear_move(&mut self, to_clear: &Move) {
        self[to_clear.from].remove_update();
        self[to_clear.to].remove_update();
    }

    pub fn show_moves(&mut self, possible_moves: &PossibleMoves) {
        for possible_move in &possible_moves.0 {

            match possible_move.typ {
                MoveType::Normal => {
                    self[possible_move.to].set_highlight();
                },
                MoveType::Capture => {
                    self[possible_move.to].set_warn();
                },
                MoveType::PawnDoubleMove => {
                    self[possible_move.to].set_highlight();
                },
                MoveType::EnPassant => {
                    self[possible_move.to].set_warn();
                },
                MoveType::KingSideCastling => {
                    self[possible_move.to].set_highlight();
                },
                MoveType::QueenSideCastling => {
                    self[possible_move.to].set_highlight();
                },
                MoveType::PawnPromotion(_) => {
                    self[possible_move.to].set_highlight();
                },
            }
        }
    }

    pub fn clear_moves(&mut self, possible_moves: &PossibleMoves) {
        for possible_move in &possible_moves.0 {
            match possible_move.typ {
                MoveType::Normal => {
                    self[possible_move.to].remove_highlight();
                },
                MoveType::Capture => {
                    self[possible_move.to].remove_warn();
                },
                MoveType::PawnDoubleMove => {
                    self[possible_move.to].remove_highlight();
                },
                MoveType::EnPassant => {
                    self[possible_move.to].remove_warn();
                },
                MoveType::KingSideCastling => {
                    self[possible_move.to].remove_highlight();
                },
                MoveType::QueenSideCastling => {
                    self[possible_move.to].remove_highlight();
                },
                MoveType::PawnPromotion(_) => {
                    self[possible_move.to].remove_highlight();
                },
            }
        }
    }
}

impl PossibleMoves {
    pub fn match_move_with_dest_index(&self, index: CellIndex) -> Option<Move> {
        for possibility in &self.0 {
            if possibility.to == index {
                return Some(possibility.clone());
            }
        }

        None
    }
}

#[bitflags]
#[repr(u8)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum BoardStateInternal {
    WhiteKingSideCastlingAllowed,
    WhiteQueenSideCastlingAllowed,

    BlackKingSideCastlingAllowed,
    BlackQueenSideCastlingAllowed,

    WhiteCheck,
    WhiteWon,

    BlackCheck,
    BlackWon,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct BoardState(BitFlags<BoardStateInternal>);

impl BoardState {
    pub fn new_game_state() -> Self {
        Self (make_bitflags!(BoardStateInternal::{
            WhiteKingSideCastlingAllowed  |
            WhiteQueenSideCastlingAllowed |
            BlackKingSideCastlingAllowed  |
            BlackQueenSideCastlingAllowed
        }))
    }
}


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct GameState {
    cells: Board<Cell>,
    turn: Color,
    board_state: BoardState,
    en_passant_cell: Option<CellIndex>,
}

impl GameState {
    pub fn new(cells: Board<Cell>, turn: Color, board_state: BoardState, en_passant_cell: Option<CellIndex>) -> Self {
        Self {
            cells,
            turn,
            board_state,
            en_passant_cell,
        }
    }

    pub fn default_starting_game_state() -> Self {
        Self::new(Board::<Cell>::start_position(), Color::White, BoardState::new_game_state(), None)
    }

    pub fn cells(&self) -> &Board<Cell> {
        &self.cells
    }

    pub fn turn(&self) -> Color {
        self.turn
    }

    pub fn apply_move(&mut self, to_apply: &Move) {
        validate!(!(self.board_state.0.contains(BoardStateInternal::WhiteWon) && self.board_state.0.contains(BoardStateInternal::BlackWon)),
            "Can only make a move when game is in play");

        let src_cell = self.cells[to_apply.from];
        let dest_cell = self.cells[to_apply.to];

        let mut king_moved = false;
        let mut king_side_rook_moved = false;
        let mut queen_side_rook_moved = false;

        macro_rules! update_if_king_or_rook_moved {
            () => {
                {
                    if src_cell.piece_typ() == PieceType::King { king_moved = true; }

                    if src_cell.piece_typ() == PieceType::Rook {
                        if to_apply.from.is_file('h') { king_side_rook_moved = true; }
                        if to_apply.from.is_file('a') { queen_side_rook_moved = true; }
                    }
                }
            }
        }

        match to_apply.typ {
            MoveType::Normal => {
                validate!(dest_cell.is_empty(), "Normal move is not supposed to be a capture");
                self.cells[to_apply.to] = src_cell;
                self.cells[to_apply.from] = piece!();

                self.en_passant_cell = None;

                update_if_king_or_rook_moved!()
            },
            MoveType::Capture => {
                validate!(!dest_cell.is_empty(), "Capture move is supposed to have a piece at destination");
                self.cells[to_apply.to] = src_cell;
                self.cells[to_apply.from] = piece!();

                self.en_passant_cell = None;

                update_if_king_or_rook_moved!()
            },
            MoveType::PawnDoubleMove => {
                validate!(dest_cell.is_empty(), "PawnDoubleMove move is not supposed to have a piece at destination");
                validate!(src_cell.is(Cell::piece(PieceType::Pawn, self.turn)), "PawnDoubleMove move source is supposed to be a pawn");

                let in_between_index = match self.turn {
                    Color::Black => {
                        validate!(to_apply.from.down().unwrap().down().unwrap() == to_apply.to, "PawnDoubleMove move is supposed to be 2 moves forward");
                        validate!(to_apply.from.is_rank('7'), "PawnDoubleMove move is supposed to start from rank '7'");
                        to_apply.from.down().unwrap()
                    },
                    Color::White => {
                        validate!(to_apply.from.up().unwrap().up().unwrap() == to_apply.to, "PawnDoubleMove move is supposed to be 2 moves forward");
                        validate!(to_apply.from.is_rank('2'), "PawnDoubleMove move is supposed to start from rank '2'");
                        to_apply.from.up().unwrap()
                    },
                };

                let in_between_cell = self.cells[in_between_index];
                validate!(in_between_cell.is_empty(), "PawnDoubleMove move is supposed to have no items in between src and dest");

                self.cells[to_apply.to] = src_cell;
                self.cells[to_apply.from] = piece!();

                self.en_passant_cell = Some(to_apply.to);
            },
            MoveType::EnPassant => {
                validate!(dest_cell.is_empty(), "EnPassant move is not supposed to have a piece at destination");
                validate!(src_cell.is(Cell::piece(PieceType::Pawn, self.turn)), "EnPassant move source is supposed to be a pawn");
                validate!(self.en_passant_cell.is_some(), "EnPassant move source is supposed to be a played after a double forward pawn move");

                let last_double_forward_pawn_cell_index = self.en_passant_cell.unwrap();
                let attack_cell = self.cells[last_double_forward_pawn_cell_index];

                validate!(attack_cell.is(Cell::piece(PieceType::Pawn, self.turn.flip())), "EnPassant attack cell is supposed to be a pawn of opposite color");

                validate!(last_double_forward_pawn_cell_index.left() == Some(to_apply.from) || last_double_forward_pawn_cell_index.right() == Some(to_apply.from),
                    "EnPassant move is supposed to be played on pawn immediately next to last turn double moved");

                match self.turn {
                    Color::Black => {
                        validate!(last_double_forward_pawn_cell_index.down().unwrap() == to_apply.to, "EnPassant move is supposed to be a diagonal move");
                        validate!(last_double_forward_pawn_cell_index.is_rank('4'), "EnPassant move is supposed to start from rank '4'");
                    },
                    Color::White => {
                        validate!(last_double_forward_pawn_cell_index.up().unwrap() == to_apply.to, "EnPassant move is supposed to be a diagonal move");
                        validate!(last_double_forward_pawn_cell_index.is_rank('5'), "EnPassant move is supposed to start from rank '5'");
                    },
                }

                self.cells[to_apply.to] = src_cell;
                self.cells[to_apply.from] = piece!();
                self.cells[last_double_forward_pawn_cell_index] = piece!();

                self.en_passant_cell = None;
            },
            MoveType::PawnPromotion(p) => {
                validate!(src_cell.is(Cell::piece(PieceType::Pawn, self.turn)), "PawnPromotion move source is supposed to be a pawn");

                match self.turn {
                    Color::Black => {
                        validate!(to_apply.to.is_rank('1'), "PawnPromotion is supposed to bhappen on last rank");
                    },
                    Color::White => {
                        validate!(to_apply.to.is_rank('8'), "PawnPromotion is supposed to happen on last rank");
                    },
                }

                self.cells[to_apply.to] = Cell::piece(p.unwrap(), self.turn);
                self.cells[to_apply.from] = piece!();
                self.en_passant_cell = None;
            },
            MoveType::KingSideCastling => {
                validate!(dest_cell.is_empty(), "King side castling move is not supposed to have a piece at destination");
                validate!(src_cell.is(Cell::piece(PieceType::King, self.turn)), "King side castling move source is supposed to be a pawn");

                let (rook_from_index, rook_to_index) = match self.turn {
                    Color::Black => {
                        validate!(to_apply.from == CellIndex::from('e', '8'), "King side castling requires king to be in its original location");
                        validate!(to_apply.to == CellIndex::from('g', '8'), "King side castling requires king to go to g8");

                        validate!(self.board_state.0.contains(BoardStateInternal::BlackKingSideCastlingAllowed), "King side castling is not allowed");
                        validate!(!self.board_state.0.contains(BoardStateInternal::BlackCheck), "King side castling is not allowed when in check");

                        validate!(self.cells[CellIndex::from('f', '8')].is_empty(), "King side castling requires next cell to be empty to be empty");

                        (CellIndex::from('h', '8'), CellIndex::from('f', '8'))
                    },
                    Color::White => {
                        validate!(to_apply.from == CellIndex::from('e', '1'), "King side castling requires king to be in its original location");
                        validate!(to_apply.to == CellIndex::from('g', '1'), "King side castling requires king to go g1");

                        validate!(self.board_state.0.contains(BoardStateInternal::WhiteKingSideCastlingAllowed), "King side castling is not allowed");
                        validate!(!self.board_state.0.contains(BoardStateInternal::WhiteCheck), "King side castling is not allowed when in check");

                        validate!(self.cells[CellIndex::from('f', '1')].is_empty(), "King side castling requires next cell to be empty to be empty");

                        (CellIndex::from('h', '1'), CellIndex::from('f', '1'))
                    },
                };

                let rook_src_cell = self.cells[rook_from_index];

                validate!(rook_src_cell.is(Cell::piece(PieceType::Rook, self.turn)), "King side castling move requires Rook to be it its original location");

                self.cells[to_apply.to] = src_cell;
                self.cells[to_apply.from] = piece!();

                self.cells[rook_to_index] = rook_src_cell;
                self.cells[rook_from_index] = piece!();

                self.en_passant_cell = None;
                king_moved = true;
            },
            MoveType::QueenSideCastling => {
                validate!(dest_cell.is_empty(), "Queen side castling move is not supposed to have a piece at destination");
                validate!(src_cell.is(Cell::piece(PieceType::King, self.turn)), "Queen side castling move source is supposed to be a pawn");

                let (rook_from_index, rook_to_index) = match self.turn {
                    Color::Black => {
                        validate!(to_apply.from == CellIndex::from('e', '8'), "Queen side castling requires king to be in its original location");
                        validate!(to_apply.to == CellIndex::from('c', '8'), "Queen side castling requires king to go to c8");

                        validate!(self.cells[CellIndex::from('d', '8')].is_empty(), "Queen side castling requires next cell to be empty to be empty");
                        validate!(self.cells[CellIndex::from('b', '8')].is_empty(), "Queen side castling requires cell b8 to be empty to be empty");

                        validate!(self.board_state.0.contains(BoardStateInternal::BlackQueenSideCastlingAllowed), "Queen side castling is not allowed now");
                        validate!(!self.board_state.0.contains(BoardStateInternal::BlackCheck), "Queen side castling is not allowed when in check");

                        (CellIndex::from('a', '8'), CellIndex::from('d', '8'))
                    },
                    Color::White => {
                        validate!(to_apply.from == CellIndex::from('e', '1'), "Queen side castling requires king to be in its original location");
                        validate!(to_apply.to == CellIndex::from('c', '1'), "Queen side castling requires king to go to c1");

                        validate!(self.cells[CellIndex::from('d', '1')].is_empty(), "Queen side castling requires next cell to be empty to be empty");
                        validate!(self.cells[CellIndex::from('b', '1')].is_empty(), "Queen side castling requires cell b1 to be empty to be empty");

                        validate!(self.board_state.0.contains(BoardStateInternal::WhiteQueenSideCastlingAllowed), "Queen side castling is not allowed now");
                        validate!(!self.board_state.0.contains(BoardStateInternal::WhiteCheck), "Queen side castling is not allowed when in check");

                        (CellIndex::from('a', '1'), CellIndex::from('d', '1'))
                    },
                };

                let rook_src_cell = self.cells[rook_from_index];

                validate!(rook_src_cell.is(Cell::piece(PieceType::Rook, self.turn)), "Queen side castling move requires Rook to be it its original location");

                self.cells[to_apply.to] = src_cell;
                self.cells[to_apply.from] = piece!();

                self.cells[rook_to_index] = rook_src_cell;
                self.cells[rook_from_index] = piece!();

                self.en_passant_cell = None;
                king_moved = true;
            },
        }

        if king_moved {
            match self.turn {
                Color::White => self.board_state.0.remove(make_bitflags!(BoardStateInternal::{WhiteKingSideCastlingAllowed | WhiteQueenSideCastlingAllowed})),
                Color::Black => self.board_state.0.remove(make_bitflags!(BoardStateInternal::{BlackKingSideCastlingAllowed | BlackQueenSideCastlingAllowed})),
            }
        } else if king_side_rook_moved {
            match self.turn {
                Color::White => self.board_state.0.remove(BoardStateInternal::WhiteKingSideCastlingAllowed),
                Color::Black => self.board_state.0.remove(BoardStateInternal::BlackKingSideCastlingAllowed),
            }
        } else if queen_side_rook_moved {
            match self.turn {
                Color::White => self.board_state.0.remove(BoardStateInternal::WhiteQueenSideCastlingAllowed),
                Color::Black => self.board_state.0.remove(BoardStateInternal::BlackQueenSideCastlingAllowed),
            }
        }

        // Cells are all updated. Need to analyze king positions to determine the state of the game
        match self.analyse_kings_positions() {
            KingPositions::Secure => {
                self.board_state.0.remove(BoardStateInternal::BlackCheck);
                self.board_state.0.remove(BoardStateInternal::WhiteCheck);
            },
            KingPositions::Vulnerable { side, .. } => {
                match side {
                    Color::Black => self.board_state.0.insert(BoardStateInternal::BlackCheck),
                    Color::White => self.board_state.0.insert(BoardStateInternal::WhiteCheck),
                }
            },
            KingPositions::Defeated { side, .. } => {
                match side {
                    Color::Black => self.board_state.0.insert(BoardStateInternal::BlackWon),
                    Color::White => self.board_state.0.insert(BoardStateInternal::WhiteWon),
                }
            },
        }

        // Then flip the turn
        self.turn.toggle()
    }

    fn is_valid_move(&self, to_test: &Move) -> bool {
        let mut self_copy = self.clone();
        self_copy.apply_move(to_test);

        match self_copy.analyse_kings_positions() {
            KingPositions::Secure => true,
            KingPositions::Vulnerable { side, .. } => {
                side != self.turn
            },
            KingPositions::Defeated { side, .. } => {
                side != self.turn
            },
        }
    }

    pub fn generate_possible_moves(&self, index: CellIndex) -> PossibleMoves {
        let cell = self.cells[index];
        let mut possibilities = Vec::new();

        macro_rules! check_add_possibile_move {
            ($to_add:expr) => {
                if self.is_valid_move(&$to_add) {
                    possibilities.push($to_add);
                }
            }

        }

        if !cell.is_empty() && cell.piece_color() == self.turn {
            match cell.piece_typ() {
                PieceType::Rook => {
                    for move_typ in [PieceMoveIterType::Up, PieceMoveIterType::Left, PieceMoveIterType::Down, PieceMoveIterType::Right] {
                        for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(index, move_typ) {
                            match typ {
                                CellType::Empty => {
                                    check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Normal });
                                },
                                CellType::Occupied => {
                                    if iter_cell.piece_color() != self.turn {
                                        check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Capture });
                                    }
                                    break;
                                },
                                _ => unreachable!()
                            }
                        }
                    }
                },
                PieceType::Bishop => {
                    for move_typ in [PieceMoveIterType::UpLeft, PieceMoveIterType::DownLeft, PieceMoveIterType::DownRight, PieceMoveIterType::UpRight] {
                        for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(index, move_typ) {
                            match typ {
                                CellType::Empty => {
                                    check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Normal });
                                },
                                CellType::Occupied => {
                                    if iter_cell.piece_color() != self.turn {
                                        check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Capture });
                                    }
                                    break;
                                },
                                _ => unreachable!()
                            }
                        }
                    }
                },
                PieceType::Queen => {
                    for move_typ in [PieceMoveIterType::Up, PieceMoveIterType::UpLeft, PieceMoveIterType::Left, PieceMoveIterType::DownLeft,
                        PieceMoveIterType::Down, PieceMoveIterType::DownRight, PieceMoveIterType::Right, PieceMoveIterType::UpRight] {

                        for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(index, move_typ) {
                            match typ {
                                CellType::Empty => {
                                    check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Normal });
                                },
                                CellType::Occupied => {
                                    if iter_cell.piece_color() != self.turn {
                                        check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Capture });
                                    }
                                    break;
                                },
                                _ => unreachable!()
                            }
                        }
                    }
                },
                PieceType::Knight => {
                    for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(index, PieceMoveIterType::Knight) {
                        match typ {
                            CellType::Empty => {
                                check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Normal });
                            },
                            CellType::Occupied => {
                                if iter_cell.piece_color() != self.turn {
                                    check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Capture });
                                }
                            },
                            _ => unreachable!()
                        }
                    }
                },
                PieceType::King => {
                    let move_type = match self.turn {
                        Color::Black => PieceMoveIterType::BlackKing,
                        Color::White => PieceMoveIterType::WhiteKing,
                    };

                    macro_rules! check_add_possible_castling {
                        ($board_state:ident, $dir:ident, $typ:ident) => {
                            if self.board_state.0.contains(BoardStateInternal::$board_state) {
                                let currently_in_check = match self.analyse_kings_positions() {
                                    KingPositions::Secure => false,
                                    KingPositions::Vulnerable { side, .. } => side == self.turn,
                                    KingPositions::Defeated { .. } => unreachable!(),
                                };

                                if currently_in_check { continue; }

                                if !self.is_valid_move(&Move { from: index, to: index.$dir().unwrap(), typ: MoveType::Normal }) {
                                    continue;
                                }

                                let mut king_move = Move { from: index, to: index.$dir().unwrap().$dir().unwrap(), typ: MoveType::Normal };
                                if !self.is_valid_move(&king_move) {
                                    continue;
                                }

                                king_move.typ = MoveType::$typ;
                                possibilities.push(king_move);
                            }
                        }
                    }

                    for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(index, move_type) {
                        match typ {
                            CellType::Empty => {
                                check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Normal });
                            },
                            CellType::Occupied => {
                                if iter_cell.piece_color() != self.turn {
                                    check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Capture });
                                }
                            },
                            CellType::KingSideCastling => {
                                // King should not be in check, cross any square which can be attacked by enemy or
                                // end in a square attacked by enemy
                                match self.turn {
                                    Color::Black => {
                                        check_add_possible_castling!(BlackKingSideCastlingAllowed, right, KingSideCastling);
                                    },
                                    Color::White => {
                                        check_add_possible_castling!(WhiteKingSideCastlingAllowed, right, KingSideCastling);
                                    },
                                }
                            },
                            CellType::QueenSideCastling => {
                                match self.turn {
                                    Color::Black => {
                                        check_add_possible_castling!(BlackQueenSideCastlingAllowed, left, QueenSideCastling);
                                    },
                                    Color::White => {
                                        check_add_possible_castling!(WhiteQueenSideCastlingAllowed, left, QueenSideCastling);
                                    }
                                }
                            },
                            _ => unreachable!()
                        }
                    }
                },
                PieceType::Pawn => {
                    let move_type = match self.turn {
                        Color::Black => PieceMoveIterType::BlackPawn,
                        Color::White => PieceMoveIterType::WhitePawn,
                    };

                    for (_, typ, iter_index) in self.cells.iterate_over_moves_from_cell(index, move_type) {
                        match typ {
                            CellType::Empty => {
                                check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Normal });
                            },
                            CellType::Occupied => {
                                check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::Capture });
                            },
                            CellType::DoubleForward => {
                                check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::PawnDoubleMove });
                            },
                            CellType::EnPassant => {
                                check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::EnPassant });
                            },
                            CellType::Promotion => {
                                check_add_possibile_move!(Move { from: index, to: iter_index, typ: MoveType::PawnPromotion(None) });
                            },
                            _ => unreachable!(),
                        }
                    }
                },
            }
        }

        PossibleMoves(possibilities)
    }
    
    pub fn apply_san_move(&mut self, san: &str) {

    //    //println!("{board}   {}", match turn { Color::White => san.white(), Color::Black => san.blue() });

        let mut chars = san.chars().peekable();

        let piece = {
            let ch = chars.peek().unwrap();

            match ch {
                'K' => {
                    chars.next();
                    Cell::piece(PieceType::King, self.turn)
                },
                'Q' => {
                    chars.next();
                    Cell::piece(PieceType::Queen, self.turn)
                },
                'B' => {
                    chars.next();
                    Cell::piece(PieceType::Bishop, self.turn)
                },
                'N' => {
                    chars.next();
                    Cell::piece(PieceType::Knight, self.turn)
                },
                'R' => {
                    chars.next();
                    Cell::piece(PieceType::Rook, self.turn)
                },
                'O' | '0' => {
                    let c = chars.next().unwrap();
                    assert!(chars.next() == Some('-'));
                    assert!(chars.next() == Some(c));

                    if let Some(next_ch) = chars.peek().cloned() {
                        if next_ch == '+' || next_ch == '#' {
                            chars.next();
                        }
                    };

                    match chars.peek() {
                        Some(_) => {
                            assert!(chars.next() == Some('-'));
                            assert!(chars.next() == Some(c));

                            match self.turn {
                                Color::Black => {
                                    self.apply_move(&Move { from: CellIndex::from('e', '8'), to: CellIndex::from('c', '8'), typ: MoveType::QueenSideCastling });
                                },
                                Color::White => {
                                    self.apply_move(&Move { from: CellIndex::from('e', '1'), to: CellIndex::from('c', '1'), typ: MoveType::QueenSideCastling });
                                },
                            }
                        },
                        None => {
                            match self.turn {
                                Color::Black => {
                                    self.apply_move(&Move { from: CellIndex::from('e', '8'), to: CellIndex::from('g', '8'), typ: MoveType::KingSideCastling });
                                },
                                Color::White => {
                                    self.apply_move(&Move { from: CellIndex::from('e', '1'), to: CellIndex::from('g', '1'), typ: MoveType::KingSideCastling });
                                },
                            }
                        },
                    }
                    return;
                }
                _ => Cell::piece(PieceType::Pawn, self.turn),
            }
        };

        let mut src_col = {
            let ch = chars.peek().unwrap();

            match ch {
                'a' => Some(0_u8),
                'b' => Some(1),
                'c' => Some(2),
                'd' => Some(3),
                'e' => Some(4),
                'f' => Some(5),
                'g' => Some(6),
                'h' => Some(7),
                _ => None
            }
        };

        if src_col.is_some() {
            chars.next();
        }

        let mut src_row = {
            let ch = chars.peek().unwrap();

            match ch {
                '1' => Some(7_u8),
                '2' => Some(6),
                '3' => Some(5),
                '4' => Some(4),
                '5' => Some(3),
                '6' => Some(2),
                '7' => Some(1),
                '8' => Some(0),
                _ => None
            }
        };

        if src_row.is_some() {
            chars.next();
        }

        let capture = {
            let ch = chars.peek();

            match ch {
                Some(ch) => {
                    if *ch == 'x' {
                        chars.next();
                        true
                    } else {
                        false
                    }
                },
                None => false,
            }
        };

        let mut dest_col = None;
        let mut dest_row = None;

        if let Some(ch) = chars.peek().cloned() {

            dest_col = match ch {
                'a' => Some(0_u8),
                'b' => Some(1),
                'c' => Some(2),
                'd' => Some(3),
                'e' => Some(4),
                'f' => Some(5),
                'g' => Some(6),
                'h' => Some(7),
                _ => None
            };
        }

        if dest_col.is_some() {
            chars.next();
        } else {
            dest_col = src_col;
            src_col = None;
        }

        if let Some(ch) = chars.peek().cloned() {
            dest_row = match ch {
                '1' => Some(7_u8),
                '2' => Some(6),
                '3' => Some(5),
                '4' => Some(4),
                '5' => Some(3),
                '6' => Some(2),
                '7' => Some(1),
                '8' => Some(0),
                _ => None
            };
        }

        if dest_row.is_some() {
            chars.next();
        } else {
            dest_row = src_row;
            src_row = None;
        }

        let dest_index = CellIndex::from_raw(dest_row.unwrap(), dest_col.unwrap());

        let mut found_move = None;

        macro_rules! check_set_move {
            ($cell:expr, $index:expr) => {
                loop {
                    if $cell.is(piece) {
                        let (found_row, found_col) = $index.to_raw();
                        if let Some(explicit_row) = src_row {
                            if found_row != explicit_row {
                                break;
                            }
                        }

                        if let Some(explicit_col) = src_col {
                            if found_col != explicit_col {
                                break;
                            }
                        }

                        let piece_move = Move { from: $index, to: dest_index, typ: if capture { MoveType::Capture } else { MoveType::Normal } };
                        if self.is_valid_move(&piece_move) {
                            validate!(found_move.is_none(), "Found multiple possible moves. Ambiguous san movement");
                            found_move = Some(piece_move);
                        }
                    }
                    break;
                }
            }
        }

        match piece.piece_typ() {
            PieceType::Rook => {
                for move_typ in [PieceMoveIterType::Up, PieceMoveIterType::Left, PieceMoveIterType::Down, PieceMoveIterType::Right] {
                    for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(dest_index, move_typ) {
                        match typ {
                            CellType::Empty => {},
                            CellType::Occupied => {
                                check_set_move!(iter_cell, iter_index);
                                break;
                            },
                            _ => unreachable!()
                        }
                    }
                }
            },
            PieceType::Bishop => {
                for move_typ in [PieceMoveIterType::UpLeft, PieceMoveIterType::DownLeft, PieceMoveIterType::DownRight, PieceMoveIterType::UpRight] {
                    for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(dest_index, move_typ) {
                        match typ {
                            CellType::Empty => { },
                            CellType::Occupied => {
                                check_set_move!(iter_cell, iter_index);
                                break;
                            },
                            _ => unreachable!()
                        }
                    }
                }
            },
            PieceType::Queen => {
                for move_typ in [PieceMoveIterType::Up, PieceMoveIterType::UpLeft, PieceMoveIterType::Left, PieceMoveIterType::DownLeft,
                    PieceMoveIterType::Down, PieceMoveIterType::DownRight, PieceMoveIterType::Right, PieceMoveIterType::UpRight] {

                    for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(dest_index, move_typ) {
                        match typ {
                            CellType::Empty => { },
                            CellType::Occupied => {
                                check_set_move!(iter_cell, iter_index);
                                break;
                            },
                            _ => unreachable!()
                        }
                    }
                }
            },
            PieceType::Knight => {
                for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(dest_index, PieceMoveIterType::Knight) {
                    match typ {
                        CellType::Empty => { },
                        CellType::Occupied => {
                            check_set_move!(iter_cell, iter_index);
                        },
                        _ => unreachable!()
                    }
                }
            },
            PieceType::King => {
                let move_type = match self.turn {
                    Color::Black => PieceMoveIterType::BlackKing,
                    Color::White => PieceMoveIterType::WhiteKing,
                };

                for (iter_cell, typ, iter_index) in self.cells.iterate_over_moves_from_cell(dest_index, move_type) {
                    match typ {
                        CellType::Empty => { },
                        CellType::Occupied => {
                            check_set_move!(iter_cell, iter_index);
                        },
                        // Cannot happen here. Castlings have different format
                        CellType::KingSideCastling => { },
                        CellType::QueenSideCastling => { },
                        _ => unreachable!()
                    }
                }
            },
            PieceType::Pawn => {
                macro_rules! check_set_pawn_move {
                    ($index:expr, $typ:expr, Expr) => {
                        loop {
                            if let Some(index) = $index {

                                let (found_row, found_col) = index.to_raw();
                                if let Some(explicit_row) = src_row {
                                    if found_row != explicit_row {
                                        break;
                                    }
                                }

                                if let Some(explicit_col) = src_col {
                                    if found_col != explicit_col {
                                        break;
                                    }
                                }

                                if self.cells()[index].is(piece) {
                                    let piece_move = Move { from: index, to: dest_index, typ: $typ };
                                    if self.is_valid_move(&piece_move) {
                                        validate!(found_move.is_none(), "Found multiple possible moves. Ambiguous san movement");
                                        found_move = Some(piece_move);
                                    }
                                }
                            }
                            break;
                        }
                    };
                    ($index:expr, $typ:ident) => {
                        check_set_pawn_move!($index, MoveType::$typ, Expr)
                    }
                }

                if let Some(ch) = chars.peek().cloned() {
                    if ch == '=' {
                        // pawn promotion
                        chars.next();

                        let piece = match chars.next().unwrap() {
                            'K' => PieceType::King,
                            'N' => PieceType::Knight,
                            'Q' => PieceType::Queen,
                            'R' => PieceType::Rook,
                            'B' => PieceType::Bishop,
                            _ => unreachable!(),
                        };

                        match self.turn {
                            Color::Black => {
                                if capture {
                                    check_set_pawn_move!(dest_index.up_left(), MoveType::PawnPromotion(Some(piece)), Expr);
                                    check_set_pawn_move!(dest_index.up_right(), MoveType::PawnPromotion(Some(piece)), Expr);
                                } else {
                                    check_set_pawn_move!(dest_index.up(), MoveType::PawnPromotion(Some(piece)), Expr);
                                }
                            },
                            Color::White => {
                                if capture {
                                    check_set_pawn_move!(dest_index.down_left(), MoveType::PawnPromotion(Some(piece)), Expr);
                                    check_set_pawn_move!(dest_index.down_right(), MoveType::PawnPromotion(Some(piece)), Expr);
                                } else {
                                    check_set_pawn_move!(dest_index.down(), MoveType::PawnPromotion(Some(piece)), Expr);
                                }
                            },
                        }
                    }
                }

                if found_move.is_none() {
                    if !capture {
                        match self.turn {
                            Color::Black => {
                                check_set_pawn_move!(dest_index.up(), Normal);

                                if dest_index.is_rank('5') && self.cells[dest_index.up().unwrap()].is_empty() {
                                    check_set_pawn_move!(dest_index.up().unwrap().up(), PawnDoubleMove);
                                }
                            },
                            Color::White => {
                                check_set_pawn_move!(dest_index.down(), Normal);

                                if dest_index.is_rank('4') && self.cells[dest_index.down().unwrap()].is_empty() {
                                    check_set_pawn_move!(dest_index.down().unwrap().down(), PawnDoubleMove);
                                }

                            },
                        };
                    } else {
                        let attacks = match self.turn {
                            Color::Black => {
                                [dest_index.up_left(), dest_index.up_right()]
                            },
                            Color::White => {
                                [dest_index.down_left(), dest_index.down_right()]
                            },
                        };

                        for attack_from in attacks {
                            if let Some(attack_from_index) = attack_from {

                                if self.cells()[attack_from_index].is(piece) {
                                    if self.cells()[dest_index].is_empty() {
                                        match self.turn {
                                            Color::Black => {
                                                if dest_index.is_rank('3') && self.en_passant_cell == dest_index.up() {
                                                    check_set_pawn_move!(attack_from, EnPassant)
                                                }
                                            },
                                            Color::White => {
                                                if dest_index.is_rank('6') && self.en_passant_cell == dest_index.down() {
                                                    check_set_pawn_move!(attack_from, EnPassant)
                                                }
                                            },
                                        }
                                    } else if self.cells()[dest_index].piece_color() != self.turn {
                                        check_set_pawn_move!(attack_from, Capture);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        validate!(found_move.is_some(), "No possible move found for the given san movement");
        self.apply_move(&found_move.unwrap());
    }

    // Analyses the board for king positions for Checks and Checkmates
    // Prioritizes other's king over the self.turn's
    fn analyse_kings_positions(&self) -> KingPositions {
         let mut first_king_index = None;
         let mut second_king_index = None;

        for index in CellIndexIter::new() {
            let piece = self.cells[index];

            if !piece.is_empty() {
                if piece.piece_typ() != PieceType::King { continue; }

                if piece.piece_color() == self.turn.flip() {
                    first_king_index = Some(index);
                } else {
                    second_king_index = Some(index);
                }

                if first_king_index.is_some() && second_king_index.is_some() {
                    break;
                }
            }
        }

        let first_king_index = first_king_index.unwrap();
        let second_king_index = second_king_index.unwrap();

        let mut king_position = KingPositions::Secure;

        for (king_index, king_color) in [(first_king_index, self.turn.flip()), (second_king_index, self.turn)] {

            let enemy_color = king_color.flip();

            'queen_rook_search:
            for move_typ in [PieceMoveIterType::Up, PieceMoveIterType::Left, PieceMoveIterType::Down, PieceMoveIterType::Right] {

                for (cell, typ, index) in self.cells.iterate_over_moves_from_cell(king_index, move_typ) {
                    match typ {
                        CellType::Empty => {},
                        CellType::Occupied => {
                            if cell.piece_color() == enemy_color && [PieceType::Rook, PieceType::Queen].contains(&cell.piece_typ()) {
                                king_position = KingPositions::Vulnerable { side: king_color, king_cell: king_index, attack_index: index };
                                break 'queen_rook_search;
                            }

                            // If the cell is occupied, rook/queen cannot pass
                            break;
                        },
                        _ => unreachable!()
                    }
                }
            }

            if king_position == KingPositions::Secure {
                'queen_bishop_search:
                for move_typ in [PieceMoveIterType::UpLeft, PieceMoveIterType::DownLeft, PieceMoveIterType::DownRight, PieceMoveIterType::UpRight] {

                    for (cell, typ, index) in self.cells.iterate_over_moves_from_cell(king_index, move_typ) {
                        match typ {
                            CellType::Empty => {},
                            CellType::Occupied => {
                                if cell.piece_color() == enemy_color && [PieceType::Bishop, PieceType::Queen].contains(&cell.piece_typ()) {
                                    king_position = KingPositions::Vulnerable { side: king_color, king_cell: king_index, attack_index: index };
                                    break 'queen_bishop_search;
                                }

                                // If the cell is occupied, bishop/queen cannot pass
                                break;
                            },
                            _ => unreachable!()
                        }
                    }
                }
            }

            if king_position == KingPositions::Secure {
                for (cell, typ, index) in self.cells.iterate_over_moves_from_cell(king_index, PieceMoveIterType::Knight) {
                    match typ {
                        CellType::Empty => {},
                        CellType::Occupied => {
                            if cell.piece_color() == enemy_color && cell.piece_typ() == PieceType::Knight {
                                king_position = KingPositions::Vulnerable { side: king_color, king_cell: king_index, attack_index: index };
                                break;
                            }
                        },
                        _ => unreachable!()
                    }
                }
            }

            if king_position == KingPositions::Secure {
                let iter_typ = match king_color {
                    Color::Black => PieceMoveIterType::BlackKing,
                    Color::White => PieceMoveIterType::WhiteKing,
                };

                for (cell, typ, index) in self.cells.iterate_over_moves_from_cell(king_index, iter_typ) {
                    match typ {
                        CellType::Empty => {},
                        CellType::Occupied => {
                            if cell.piece_color() == enemy_color && cell.piece_typ() == PieceType::King {
                                king_position = KingPositions::Vulnerable { side: king_color, king_cell: king_index, attack_index: index };
                                break;
                            }
                        },
                        CellType::KingSideCastling => { },
                        CellType::QueenSideCastling => { },
                        _ => unreachable!()
                    }
                }
            }

            if king_position == KingPositions::Secure {
                let pawn_attack_indexes = match king_color {
                    Color::Black => [king_index.down_left(), king_index.down_right()],
                    Color::White => [king_index.up_left(), king_index.up_right()],
                };

                for pawn_attack_index in pawn_attack_indexes.into_iter().flatten() {
                    let pawn_cell = self.cells[pawn_attack_index];

                    if pawn_cell.is(Cell::piece(PieceType::Pawn, enemy_color)) {
                        king_position = KingPositions::Vulnerable { side: king_color, king_cell: king_index, attack_index: pawn_attack_index };
                        break;
                    }
                }
            }

            if king_position != KingPositions::Secure { break; }
        }

        king_position
    }
}

