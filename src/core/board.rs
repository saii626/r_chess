use std::fmt;

use colored::Colorize;
use enumflags2::{bitflags, BitFlags};

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub enum PieceType {
    Rook,
    Knight,
    Bishop,
    Queen,
    King,
    Pawn
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub enum Color {
    Black,
    White
}

impl Color {
    pub const fn flip(&self) -> Self {
        match self {
            Color::Black => Color::White,
            Color::White => Color::Black,
        }
    }

    pub fn toggle(&mut self) {
        match self {
            Color::Black => { *self = Color::White },
            Color::White => { *self = Color::Black },
        }
    }
}

const BLACK: u8  = 1 << 0;
const WHITE: u8  = 1 << 1;

const ROOK: u8   = 1 << 2;
const KNIGHT: u8 = 1 << 3;
const BISHOP: u8 = 1 << 4;
const QUEEN: u8  = 1 << 5;
const KING: u8   = 1 << 6;
const PAWN: u8   = 1 << 7;

#[bitflags]
#[repr(u8)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum PieceInternal {
    Black  = BLACK,
    White  = WHITE,

    Rook   = ROOK,
    Knight = KNIGHT,
    Bishop = BISHOP,
    Queen  = QUEEN,
    King   = KING,
    Pawn   = PAWN,
}

impl PieceInternal {
    const fn from_typ(typ: PieceType) -> BitFlags<PieceInternal> {
        match typ {
            PieceType::Rook => BitFlags::<PieceInternal>::from_bits_truncate_c(ROOK, BitFlags::CONST_TOKEN),
            PieceType::Knight => BitFlags::<PieceInternal>::from_bits_truncate_c(KNIGHT, BitFlags::CONST_TOKEN),
            PieceType::Bishop => BitFlags::<PieceInternal>::from_bits_truncate_c(BISHOP, BitFlags::CONST_TOKEN),
            PieceType::Queen => BitFlags::<PieceInternal>::from_bits_truncate_c(QUEEN, BitFlags::CONST_TOKEN),
            PieceType::King => BitFlags::<PieceInternal>::from_bits_truncate_c(KING, BitFlags::CONST_TOKEN),
            PieceType::Pawn => BitFlags::<PieceInternal>::from_bits_truncate_c(PAWN, BitFlags::CONST_TOKEN),
        }
    }

    const fn from_color(color: Color) -> BitFlags<PieceInternal> {
        match color {
            Color::Black => BitFlags::<PieceInternal>::from_bits_truncate_c(BLACK, BitFlags::CONST_TOKEN),
            Color::White => BitFlags::<PieceInternal>::from_bits_truncate_c(WHITE, BitFlags::CONST_TOKEN),
        }
    }

    const fn typ(piece: BitFlags<PieceInternal>) -> PieceType {
        if piece.intersection_c(Self::from_typ(PieceType::Rook)).bits_c() == Self::from_typ(PieceType::Rook).bits_c() {
            PieceType::Rook
        } else if piece.intersection_c(Self::from_typ(PieceType::Knight)).bits_c() == Self::from_typ(PieceType::Knight).bits_c() {
            PieceType::Knight
        } else if piece.intersection_c(Self::from_typ(PieceType::Bishop)).bits_c() == Self::from_typ(PieceType::Bishop).bits_c() {
            PieceType::Bishop
        } else if piece.intersection_c(Self::from_typ(PieceType::Queen)).bits_c() == Self::from_typ(PieceType::Queen).bits_c() {
            PieceType::Queen
        } else if piece.intersection_c(Self::from_typ(PieceType::King)).bits_c() == Self::from_typ(PieceType::King).bits_c() {
            PieceType::King
        } else if piece.intersection_c(Self::from_typ(PieceType::Pawn)).bits_c() == Self::from_typ(PieceType::Pawn).bits_c() {
            PieceType::Pawn
        } else {
            unreachable!()
        }
    }

    const fn color(piece: BitFlags<PieceInternal>) -> Color {
        if piece.intersection_c(Self::from_color(Color::Black)).bits_c() == Self::from_color(Color::Black).bits_c() {
            Color::Black
        } else if piece.intersection_c(Self::from_color(Color::White)).bits_c() == Self::from_color(Color::White).bits_c() {
            Color::White
        } else {
            unreachable!()
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Cell(BitFlags<PieceInternal>);

#[macro_export]
macro_rules! piece {
    ($col:ident, $typ:ident) => {
        $crate::board::Cell::piece($crate::board::PieceType::$typ, $crate::board::Color::$col)
    };
    () => {
        $crate::core::board::Cell::empty()
    }
}

impl Cell {

    pub const fn piece(typ: PieceType, color: Color) -> Self {
        Self (PieceInternal::from_typ(typ).union_c(PieceInternal::from_color(color)))
    }

    pub const fn empty() -> Self {
        Self (BitFlags::EMPTY)
    }

    pub const fn is_empty(&self) -> bool {
        self.0.bits_c() == BitFlags::<PieceInternal>::EMPTY.bits_c()
    }

    pub const fn piece_color(&self) -> Color {
        PieceInternal::color(self.0)
    }

    pub const fn piece_typ(&self) -> PieceType {
        PieceInternal::typ(self.0)
    }

    pub const fn is(&self, cell: Cell) -> bool {
        self.0.bits_c() == cell.0.bits_c()
    }
}


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Board<T> ([[T; 8]; 8]);

impl<T> Board<T> {
    pub const fn new(inner: [[T; 8]; 8]) -> Self {
        Self (inner)
    }
}

impl<T> std::ops::Index<CellIndex> for Board<T> {
    type Output = T;

    fn index(&self, index: CellIndex) -> &Self::Output {
        &self.0[index.row as usize][index.col as usize]
    }
}

impl<T> std::ops::IndexMut<CellIndex> for Board<T> {
    fn index_mut(&mut self, index: CellIndex) -> &mut Self::Output {
        &mut self.0[index.row as usize][index.col as usize]
    }
}

impl Board<Cell> {
    //pub const fn empty() -> Self {
    //    Self ([
    //          [piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!()],
    //          [piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!()],
    //          [piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!()],
    //          [piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!()],
    //          [piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!()],
    //          [piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!()],
    //          [piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!()],
    //          [piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!(), piece!()]
    //    ])
    //}

    pub const fn start_position() -> Self {

        macro_rules! black {
            ($typ:ident) => {
                $crate::core::board::Cell::piece($crate::core::board::PieceType::$typ, $crate::core::board::Color::Black)
            }
        }

        macro_rules! white {
            ($typ:ident) => {
                $crate::core::board::Cell::piece($crate::core::board::PieceType::$typ, $crate::core::board::Color::White)
            }
        }
        Self ([
              [black!(Rook), black!(Knight), black!(Bishop), black!(Queen), black!(King), black!(Bishop), black!(Knight), black!(Rook)],
              [black!(Pawn), black!(Pawn),   black!(Pawn),   black!(Pawn),  black!(Pawn), black!(Pawn),   black!(Pawn),   black!(Pawn)],
              [piece!(),     piece!(),       piece!(),       piece!(),      piece!(),     piece!(),       piece!(),       piece!()],
              [piece!(),     piece!(),       piece!(),       piece!(),      piece!(),     piece!(),       piece!(),       piece!()],
              [piece!(),     piece!(),       piece!(),       piece!(),      piece!(),     piece!(),       piece!(),       piece!()],
              [piece!(),     piece!(),       piece!(),       piece!(),      piece!(),     piece!(),       piece!(),       piece!()],
              [white!(Pawn), white!(Pawn),   white!(Pawn),   white!(Pawn),  white!(Pawn), white!(Pawn),   white!(Pawn),   white!(Pawn)],
              [white!(Rook), white!(Knight), white!(Bishop), white!(Queen), white!(King), white!(Bishop), white!(Knight), white!(Rook)],
        ])
    }

    pub fn iterate_over_moves_from_cell(&self, index: CellIndex, typ: PieceMoveIterType) -> CellsIndexIter<'_> {
        //assert!(typ != PieceMoveIterType::King && typ != PieceMoveIterType::Pawn && typ != PieceMoveIterType::PawnToPosition);

        CellsIndexIter {
            cells: self,
            typ,
            pos: index,
            index: 0,
        }
    }
}

impl fmt::Display for Board<Cell> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "{}", "\n----------".red())?;
        for row in 0..8 {
            write!(f, "{}", "|".red())?;
            for col in 0..8 {

                let cell = self.0[row][col];

                let ch = if !cell.is_empty() {
                    let piece = match cell.piece_typ() {
                        PieceType::Rook => "R",
                        PieceType::Knight => "N",
                        PieceType::Bishop => "B",
                        PieceType::Queen => "Q",
                        PieceType::King => "K",
                        PieceType::Pawn => "P",
                    };

                    match cell.piece_color() {
                        Color::Black => piece.to_lowercase().blue(),
                        Color::White => piece.yellow(),
                    }
                } else {
                    " ".into()
                };

                write!(f, "{ch}")?;
            }

            let row_char = match row {
                0 => "8",
                1 => "7",
                2 => "6",
                3 => "5",
                4 => "4",
                5 => "3",
                6 => "2",
                7 => "1",
                _ => unreachable!()
            };
            writeln!(f, "{} {}", "|".red(), row_char.green())?;
        }
        writeln!(f, "{}", "----------".red())?;
        write!(f, "{}", " abcdefgh ".green())
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum CellType {
    /// Cell is empty
    Empty,
    /// Cell is occupied by a piece
    Occupied,

    /// Pawn's double foward from starting rank
    DoubleForward,
    /// Pawn's attack and capture passed over opponent pawn
    EnPassant,
    /// Pawn's attack and capture diagnol pawn
    Promotion,

    /// King's castling with king side rook
    KingSideCastling,
    /// King's castling with Queen side rook
    QueenSideCastling,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum PieceMoveIterType {
    Up,
    Down,
    Left,
    Right,

    UpLeft,
    UpRight,
    DownRight,
    DownLeft,

    Knight,

    WhiteKing,
    BlackKing,

    WhitePawn,
    BlackPawn,
}

pub struct CellsIndexIter<'a> {
    cells: &'a Board<Cell>,
    typ: PieceMoveIterType,

    pos: CellIndex,
    index: usize,
}


impl<'a> Iterator for CellsIndexIter<'a> {
    type Item = (Cell, CellType, CellIndex);

    #[allow(unused_assignments)]
    fn next(&mut self) -> Option<Self::Item> {
        self.index += 1;

        macro_rules! unwrap_or_next {
            ($opt:expr) => {
                match $opt {
                    Some(i) => i,
                    None => { return self.next(); }
                }
            }
        }

        macro_rules! cell_iter {
            ($index:expr) => {
                {
                    let cell = self.cells[$index];
                    let typ = if cell.is_empty() {
                        CellType::Empty
                    } else {
                        CellType::Occupied
                    };

                    Some((cell, typ, $index))
                }
            }
        }

        macro_rules! next_dir_along {
            ($dir:ident) => {
                {
                    let mut new_index = self.pos;

                    for _ in 0..self.index {
                        new_index = new_index.$dir()?;
                    }

                    cell_iter!(new_index)
                }
            };

            [ $($dir:ident),+, [$($extra:block),*] ] => {
                {
                    let mut iter_index = 0;

                    $(
                        {
                            if iter_index == self.index - 1 {
                                return self.pos.$dir()
                                    .and_then(|i| cell_iter!(i))
                                    .or_else(|| self.next());
                            }

                            iter_index += 1;
                        }
                    )*

                    $(
                        {
                            if iter_index == self.index - 1 {
                                return $extra.or_else(|| self.next());
                            }

                            iter_index += 1;
                        }
                    )*

                    None
                }
            }
        }

        match self.typ {
            PieceMoveIterType::Up => next_dir_along!(up),
            PieceMoveIterType::Down => next_dir_along!(down),
            PieceMoveIterType::Left => next_dir_along!(left),
            PieceMoveIterType::Right => next_dir_along!(right),
            PieceMoveIterType::UpLeft => next_dir_along!(up_left),
            PieceMoveIterType::UpRight => next_dir_along!(up_right),
            PieceMoveIterType::DownRight => next_dir_along!(down_right),
            PieceMoveIterType::DownLeft => next_dir_along!(down_left),
            PieceMoveIterType::Knight => next_dir_along![up_up_left, up_left_left, down_left_left, down_down_left, down_down_right, down_right_right, up_right_right, up_up_right, []],
            PieceMoveIterType::WhiteKing => {
                next_dir_along![
                    up, up_left, left, down_left, down, down_right, right, up_right, [{

                    if self.pos != CellIndex::from('e', '1') { return self.next(); }

                    let r1 = unwrap_or_next!(self.pos.right());
                    if !self.cells[r1].is_empty() { return self.next(); }

                    let r2 = unwrap_or_next!(r1.right());
                    if !self.cells[r2].is_empty() { return self.next(); }

                    let r3 = unwrap_or_next!(r2.right());

                    if self.cells[r3].is(piece!(White, Rook)) {
                        return Some((piece!(), CellType::KingSideCastling, r2));
                    }

                    None
                }, {
                    if self.pos != CellIndex::from('e', '1') { return self.next(); }

                    let l1 = unwrap_or_next!(self.pos.left());
                    if !self.cells[l1].is_empty() { return self.next(); }

                    let l2 = unwrap_or_next!(l1.left());
                    if !self.cells[l2].is_empty() { return self.next(); }

                    let l3 = unwrap_or_next!(l2.left());
                    if !self.cells[l3].is_empty() { return self.next(); }

                    let l4 = unwrap_or_next!(l3.left());
                    if self.cells[l4].is(piece!(White, Rook)) {
                        return Some((piece!(), CellType::QueenSideCastling, l2));
                    }

                    None
                }]]
            },
            PieceMoveIterType::BlackKing => {
                next_dir_along![
                    up, up_left, left, down_left, down, down_right, right, up_right, [{

                    if self.pos != CellIndex::from('e', '8') { return self.next(); }

                    let r1 = unwrap_or_next!(self.pos.right());
                    if !self.cells[r1].is_empty() { return self.next(); }

                    let r2 = unwrap_or_next!(r1.right());
                    if !self.cells[r2].is_empty() { return self.next(); }

                    let r3 = unwrap_or_next!(r2.right());

                    if self.cells[r3].is(piece!(Black, Rook)) {
                        return Some((piece!(), CellType::KingSideCastling, r2));
                    }

                    None
                }, {
                    if self.pos != CellIndex::from('e', '8') { return self.next(); }

                    let l1 = unwrap_or_next!(self.pos.left());
                    if !self.cells[l1].is_empty() { return self.next(); }

                    let l2 = unwrap_or_next!(l1.left());
                    if !self.cells[l2].is_empty() { return self.next(); }

                    let l3 = unwrap_or_next!(l2.left());
                    if !self.cells[l3].is_empty() { return self.next(); }

                    let l4 = unwrap_or_next!(l3.left());
                    if self.cells[l4].is(piece!(Black, Rook)) {
                        return Some((piece!(), CellType::QueenSideCastling, l2));
                    }

                    None
                }]]
            }
            PieceMoveIterType::WhitePawn => {
                match self.index - 1 {
                    0 => {
                        // 1 step forward to empty square
                        let next_index = unwrap_or_next!(self.pos.up());

                        if self.cells[next_index].is_empty() {
                            Some((piece!(), CellType::Empty, next_index))
                        } else {
                            self.next()
                        }
                    },
                    1 => {
                        // 2 step forward from initial position
                        let next_index = {
                            if self.pos.row != 6 { return self.next(); }
                            unwrap_or_next!(self.pos.up())
                        };

                        if !self.cells[next_index].is_empty() { return self.next(); }

                        let next_next_index = unwrap_or_next!(next_index.up());

                        if self.cells[next_next_index].is_empty() {
                            Some((piece!(), CellType::DoubleForward, next_next_index))
                        } else {
                            self.next()
                        }
                    },
                    2 => {
                        // diagonally left attack on enemy piece
                        let next_index = unwrap_or_next!(self.pos.up_left());

                        let cell = self.cells[next_index];

                        if !cell.is_empty() && cell.piece_color() == Color::Black {
                            Some((piece!(), CellType::Occupied, next_index))
                        } else {
                            self.next()
                        }
                    },
                    3 => {
                        // diagonally right attack on enemy piece
                        let next_index = unwrap_or_next!(self.pos.up_right());

                        let cell = self.cells[next_index];

                        if !cell.is_empty() && cell.piece_color() == Color::Black {
                            Some((piece!(), CellType::Occupied, next_index))
                        } else {
                            self.next()
                        }
                    },
                    4 => {
                        // diagonally right en passant on enemy pawn
                        let next_index = {
                            if self.pos.row != 3 { return self.next(); }
                            unwrap_or_next!(self.pos.up_left())
                        };

                        let enemy_index = unwrap_or_next!(self.pos.left());

                        let move_cell = self.cells[next_index];
                        let enemy_cell = self.cells[enemy_index];

                        if move_cell.is_empty() && enemy_cell.is(piece!(Black, Pawn)) {
                            Some((piece!(), CellType::EnPassant, next_index))
                        } else {
                            self.next()
                        }
                    },
                    5 => {
                        // diagonally left en passant on enemy pawn
                        let next_index = {
                            if self.pos.row != 3 { return self.next(); }
                            unwrap_or_next!(self.pos.up_right())
                        };

                        let enemy_index = unwrap_or_next!(self.pos.right());

                        let move_cell = self.cells[next_index];
                        let enemy_cell = self.cells[enemy_index];

                        if move_cell.is_empty() && enemy_cell.is(piece!(Black, Pawn)) {
                            Some((piece!(), CellType::EnPassant, next_index))
                        } else {
                            self.next()
                        }
                    },
                    _ => None
                }
            },
            PieceMoveIterType::BlackPawn => {
                match self.index - 1 {
                    0 => {
                        // 1 step forward to empty square
                        let next_index = unwrap_or_next!(self.pos.down());

                        if self.cells[next_index].is_empty() {
                            Some((piece!(), CellType::Empty, next_index))
                        } else {
                            self.next()
                        }
                    },
                    1 => {
                        // 2 step forward from initial position
                        let next_index = {
                            if self.pos.row != 1 { return self.next(); }
                            unwrap_or_next!(self.pos.down())
                        };

                        if !self.cells[next_index].is_empty() { return self.next(); }

                        let next_next_index = unwrap_or_next!(next_index.down());

                        if self.cells[next_next_index].is_empty() {
                            Some((piece!(), CellType::DoubleForward, next_next_index))
                        } else {
                            self.next()
                        }
                    },
                    2 => {
                        // diagonally left attack on enemy piece
                        let next_index = unwrap_or_next!(self.pos.down_left());

                        let cell = self.cells[next_index];

                        if !cell.is_empty() && cell.piece_color() == Color::White {
                            Some((piece!(), CellType::Occupied, next_index))
                        } else {
                            self.next()
                        }
                    },
                    3 => {
                        // diagonally right attack on enemy piece
                        let next_index = unwrap_or_next!(self.pos.down_right());

                        let cell = self.cells[next_index];

                        if !cell.is_empty() && cell.piece_color() == Color::White {
                            Some((piece!(), CellType::Occupied, next_index))
                        } else {
                            self.next()
                        }
                    },
                    4 => {
                        // diagonally right en passant on enemy pawn
                        let next_index = {
                            if self.pos.row != 4 { return self.next(); }
                            unwrap_or_next!(self.pos.down_left())
                        };

                        let enemy_index = unwrap_or_next!(self.pos.left());

                        let move_cell = self.cells[next_index];
                        let enemy_cell = self.cells[enemy_index];

                        if move_cell.is_empty() && enemy_cell.is(piece!(White, Pawn)) {
                            Some((piece!(), CellType::EnPassant, next_index))
                        } else {
                            self.next()
                        }
                    },
                    5 => {
                        // diagonally left en passant on enemy pawn
                        let next_index = {
                            if self.pos.row != 4 { return self.next(); }
                            unwrap_or_next!(self.pos.down_right())
                        };

                        let enemy_index = unwrap_or_next!(self.pos.right());

                        let move_cell = self.cells[next_index];
                        let enemy_cell = self.cells[enemy_index];

                        if move_cell.is_empty() && enemy_cell.is(piece!(White, Pawn)) {
                            Some((piece!(), CellType::EnPassant, next_index))
                        } else {
                            self.next()
                        }
                    },
                    _ => None
                }
            }
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct CellIndex {
    row: u8,
    col: u8,
}


/*
 * (0,0, a8) (0,1, b8) (0,2, c8) (0,3, d8) (0,4, e8) (0,5, f8) (0,6, g8) (0,7, h8)
 * (1,0, a7) (1,1, b7) (1,2, c7) (1,3, d7) (1,4, e7) (1,5, f7) (1,6, g7) (1,7, h7)
 * (2,0, a6) (2,1, b6) (2,2, c6) (2,3, d6) (2,4, e6) (2,5, f6) (2,6, g6) (2,7, h6)
 * (3,0, a5) (3,1, b5) (3,2, c5) (3,3, d5) (3,4, e5) (3,5, f5) (3,6, g5) (3,7, h5)
 * (4,0, a4) (4,1, b4) (4,2, c4) (4,3, d4) (4,4, e4) (4,5, f4) (4,6, g4) (4,7, h4)
 * (5,0, a3) (5,1, b3) (5,2, c3) (5,3, d3) (5,4, e3) (5,5, f3) (5,6, g3) (5,7, h3)
 * (6,0, a2) (6,1, b2) (6,2, c2) (6,3, d2) (6,4, e2) (6,5, f2) (6,6, g2) (6,7, h2)
 * (7,0, a1) (7,1, b1) (7,2, c1) (7,3, d1) (7,4, e1) (7,5, f1) (7,6, g1) (7,7, h1)
 */

impl fmt::Display for CellIndex {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let row_char = match self.row {
            0 => '8',
            1 => '7',
            2 => '6',
            3 => '5',
            4 => '4',
            5 => '3',
            6 => '2',
            7 => '1',
            _ => unreachable!()
        };
        let col_char = match self.col {
            0 => 'a',
            1 => 'b',
            2 => 'c',
            3 => 'd',
            4 => 'e',
            5 => 'f',
            6 => 'g',
            7 => 'h',
            _ => unreachable!()
        };

        write!(f, "{col_char}{row_char}")
    }
}

impl CellIndex {

    pub const fn file_to_col(literal: char) -> u8 {
        match literal {
            'a' => 0,
            'b' => 1,
            'c' => 2,
            'd' => 3,
            'e' => 4,
            'f' => 5,
            'g' => 6,
            'h' => 7,
            _ => unreachable!()
        }
    }

    pub const fn rank_to_row(literal: char) -> u8 {
        match literal {
             '8' => 0,
             '7' => 1,
             '6' => 2,
             '5' => 3,
             '4' => 4,
             '3' => 5,
             '2' => 6,
             '1' => 7,
             _ => unreachable!()
        }
    }

    pub const fn from(file: char, rank: char) -> Self {
        CellIndex { row: CellIndex::rank_to_row(rank), col: CellIndex::file_to_col(file) }
    }

    pub fn from_raw(row: u8, col: u8) -> Self {
        CellIndex { row, col }
    }

    pub fn to_raw(self) -> (u8, u8) {
        (self.row, self.col)
    }

    //pub fn iter() -> Self {
    //    CellIndex { row: 0, col: 0 }
    //}

    //pub const fn end() -> Self {
    //    Self { row: 8, col: 0 }
    //}

    #[inline]
    pub fn up(self) -> Option<Self> {
        let new_row = self.row.checked_sub(1)?;
        Some(Self { row: new_row, col: self.col })
    }

    #[inline]
    pub fn down(self) -> Option<Self> {
        let new_row = self.row + 1;
        if new_row < 8 {
            Some(Self { row: new_row, col: self.col })
        } else { None }
    }

    #[inline]
    pub fn left(self) -> Option<Self> {
        let new_col = self.col.checked_sub(1)?;
        Some(Self { row: self.row, col: new_col })
    }

    #[inline]
    pub fn right(self) -> Option<Self> {
        let new_col = self.col + 1;
        if new_col < 8 {
            Some(Self { row: self.row, col: new_col })
        } else { None }
    }

    pub fn is_file(&self, file: char) -> bool {
        Self::file_to_col(file) == self.col
    }

    pub fn is_rank(&self, rank: char) -> bool {
        Self::rank_to_row(rank) == self.row
    }

    #[inline]
    pub fn up_left(self) -> Option<Self> {
        self.up()?.left()
    }

    #[inline]
    pub fn up_right(self) -> Option<Self> {
        self.up()?.right()
    }

    #[inline]
    pub fn down_right(self) -> Option<Self> {
        self.down()?.right()
    }

    #[inline]
    pub fn down_left(self) -> Option<Self> {
        self.down()?.left()
    }

    #[inline]
    fn up_up_left(self) -> Option<Self> {
        self.up()?.up()?.left()
    }

    #[inline]
    fn up_left_left(self) -> Option<Self> {
        self.up()?.left()?.left()
    }

    #[inline]
    fn down_down_left(self) -> Option<Self> {
        self.down()?.down()?.left()
    }

    #[inline]
    fn down_left_left(self) -> Option<Self> {
        self.down()?.left()?.left()
    }

    #[inline]
    fn up_up_right(self) -> Option<Self> {
        self.up()?.up()?.right()
    }

    #[inline]
    fn up_right_right(self) -> Option<Self> {
        self.up()?.right()?.right()
    }

    #[inline]
    fn down_down_right(self) -> Option<Self> {
        self.down()?.down()?.right()
    }

    #[inline]
    fn down_right_right(self) -> Option<Self> {
        self.down()?.right()?.right()
    }
}

pub struct CellIndexIter {
    index: Option<CellIndex>,
}

impl CellIndexIter {
    pub fn new() -> Self {
        CellIndexIter { index: Some(CellIndex { row: 0, col: 0 }) }
    }
}

impl Iterator for CellIndexIter {
    type Item = CellIndex;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(mut index) = self.index {
            let to_ret = index;

            self.index = {
                if index.row != 8 {
                    index.col += 1;
                    if index.col == 8 {
                        index.col = 0;
                        index.row += 1;
                    }

                    if index.row == 8 { None }
                    else { Some(index) }
                } else {
                    None
                }
            };

            Some(to_ret)
        } else { None }
    }
}


