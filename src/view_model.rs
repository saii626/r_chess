
use enumflags2::{bitflags, BitFlags};
use slint::Color;

use crate::{CellModel, CellPiece};
use crate::core::board::{self, Board, Cell, CellIndex, CellIndexIter, PieceType};

#[bitflags]
#[repr(u8)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum ViewStateInternal {
    Select,
    Update,

    Highlight,
    Warn,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct ViewState(BitFlags<ViewStateInternal>);

impl ViewState {

    pub fn set_select(&mut self) {
        self.0.insert(ViewStateInternal::Select);
    }

    pub fn remove_select(&mut self) {
        self.0.remove(ViewStateInternal::Select);
    }

    pub fn set_update(&mut self) {
        self.0.insert(ViewStateInternal::Update);
    }

    pub fn remove_update(&mut self) {
        self.0.remove(ViewStateInternal::Update);
    }

    pub fn set_highlight(&mut self) {
        self.0.insert(ViewStateInternal::Highlight);
    }

    pub fn remove_highlight(&mut self) {
        self.0.remove(ViewStateInternal::Highlight);
    }

    pub fn set_warn(&mut self) {
        self.0.insert(ViewStateInternal::Warn);
    }

    pub fn remove_warn(&mut self) {
        self.0.remove(ViewStateInternal::Warn);
    }

}

#[derive(Debug, Clone)]
pub struct BoardView {
    cells: Board<Cell>,
    colors: Board<board::Color>,
    states: Board<ViewState>,
}

impl Board<board::Color> {
    pub const fn default() -> Self {
        macro_rules! w {
            () => { board::Color::White }
        }

        macro_rules! b {
            () => { board::Color::Black }
        }

        Board::new([
               [ w!(), b!(), w!(), b!(), w!(), b!(), w!(), b!() ],
               [ b!(), w!(), b!(), w!(), b!(), w!(), b!(), w!() ],
               [ w!(), b!(), w!(), b!(), w!(), b!(), w!(), b!() ],
               [ b!(), w!(), b!(), w!(), b!(), w!(), b!(), w!() ],
               [ w!(), b!(), w!(), b!(), w!(), b!(), w!(), b!() ],
               [ b!(), w!(), b!(), w!(), b!(), w!(), b!(), w!() ],
               [ w!(), b!(), w!(), b!(), w!(), b!(), w!(), b!() ],
               [ b!(), w!(), b!(), w!(), b!(), w!(), b!(), w!() ],
        ])
    }
}

impl Board<ViewState> {
    pub const fn empty() -> Self {
        macro_rules! e {
            () => { ViewState (BitFlags::EMPTY) }
        }

        Board::new([
               [ e!(), e!(), e!(), e!(), e!(), e!(), e!(), e!() ],
               [ e!(), e!(), e!(), e!(), e!(), e!(), e!(), e!() ],
               [ e!(), e!(), e!(), e!(), e!(), e!(), e!(), e!() ],
               [ e!(), e!(), e!(), e!(), e!(), e!(), e!(), e!() ],
               [ e!(), e!(), e!(), e!(), e!(), e!(), e!(), e!() ],
               [ e!(), e!(), e!(), e!(), e!(), e!(), e!(), e!() ],
               [ e!(), e!(), e!(), e!(), e!(), e!(), e!(), e!() ],
               [ e!(), e!(), e!(), e!(), e!(), e!(), e!(), e!() ],
        ])
    }
}

impl BoardView {
    pub fn new(cells: Board<Cell>, colors: Board<board::Color>, states: Board<ViewState>) -> Self {
        Self { cells, colors, states }
    }

    pub fn cells(&self) -> &Board<Cell> {
        &self.cells
    }

    pub fn cells_mut(&mut self) -> &mut Board<Cell> {
        &mut self.cells
    }

    pub fn states(&self) -> &Board<ViewState> {
        &self.states
    }

    pub fn states_mut(&mut self) -> &mut Board<ViewState> {
        &mut self.states
    }

    pub fn get_model_at_index(&self, index: CellIndex) -> CellModel {

        let cell_piece = self.cells[index];
        let cell_color = self.colors[index];
        let cell_state = self.states[index];

        let color = match cell_color {
            board::Color::Black => Color::from_rgb_u8(101, 66, 54),
            board::Color::White => Color::from_rgb_u8(193, 193, 193),
        };

        let piece = if cell_piece.is_empty() {
            CellPiece::Empty
        } else {
            let piece_typ = cell_piece.piece_typ();
            let color = cell_piece.piece_color();

            match piece_typ {
                PieceType::Rook   => match color { board::Color::Black => CellPiece::BlackRook,   board::Color::White => CellPiece::WhiteRook },
                PieceType::Knight => match color { board::Color::Black => CellPiece::BlackKnight, board::Color::White => CellPiece::WhiteKnight },
                PieceType::Bishop => match color { board::Color::Black => CellPiece::BlackBishop, board::Color::White => CellPiece::WhiteBishop },
                PieceType::Queen  => match color { board::Color::Black => CellPiece::BlackQueen,  board::Color::White => CellPiece::WhiteQueen },
                PieceType::King   => match color { board::Color::Black => CellPiece::BlackKing,   board::Color::White => CellPiece::WhiteKing },
                PieceType::Pawn   => match color { board::Color::Black => CellPiece::BlackPawn,   board::Color::White => CellPiece::WhitePawn },
            }
        };

        let mut overlay = Color::from_rgb_u8(255, 255, 255);

        if cell_state.0 == BitFlags::EMPTY {
            overlay = overlay.transparentize(1.0);
        } else {

            if cell_state.0.contains(ViewStateInternal::Select) {
                overlay = overlay.mix(&Color::from_rgb_u8(0x00, 0xff, 0x00), 0.4);
            }

            if cell_state.0.contains(ViewStateInternal::Highlight) {
                overlay = overlay.mix(&Color::from_rgb_u8(0x00, 0x00, 0xff), 0.3);
            }

            if cell_state.0.contains(ViewStateInternal::Update) {
                overlay = overlay.mix(&Color::from_rgb_u8(0xb8, 0xf3, 0x55), 0.2);
            }

            if cell_state.0.contains(ViewStateInternal::Warn) {
                overlay = overlay.mix(&Color::from_rgb_u8(0xff, 0x00, 0x00), 0.2);
            }

            overlay = overlay.transparentize(0.7);
        }

        CellModel { color, piece, overlay }
    }

    pub fn iter_models(&self) -> CellModelsIter<'_> {
        CellModelsIter { board_view: self, index: CellIndexIter::new() }
    }
}

pub struct CellModelsIter<'a> {
    board_view: &'a BoardView,
    index: CellIndexIter,
}

impl<'a> Iterator for CellModelsIter<'a> {
    type Item = (CellIndex, CellModel);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(index) = self.index.next() {
            Some((index, self.board_view.get_model_at_index(index)))
        } else { None }
    }
}

