
use crate::core::game::{GameState, Move, PossibleMoves};
use crate::core::board::{Board, CellIndex};

use crate::utils::history::HistoryKeeper;
use crate::view_model::ViewState;
use crate::{ChessGame, Event};

use super::GameMode;

#[derive(Debug)]
enum TwoPlayerGameState {
    WaitingPieceSelection,
    PieceSelected { index: CellIndex, possible_moves: PossibleMoves },
    ViewingHistory,
}

#[derive(Debug)]
pub struct TwoPlayerMode {
    history: HistoryKeeper,
    last_move: Option<Move>,
    state: TwoPlayerGameState,
}

impl TwoPlayerMode {
    pub fn new() -> Self {
        Self {
            history: HistoryKeeper::new(),
            last_move: None,
            state: TwoPlayerGameState::WaitingPieceSelection,
        }
    }
}

impl GameMode for TwoPlayerMode {
    fn init_mode(&mut self, game_state: &GameState, view_state: &Board<ViewState>) {
        self.history.commit_state(game_state.clone(), view_state.clone());
    }

    fn handle_event(&mut self, _: Event, _: &mut GameState, _: &mut Board<ViewState>) {}

    fn handle_click(&mut self, click_index: CellIndex, game_state: &mut GameState, view_state: &mut Board<ViewState>) {
        match self.state {
            TwoPlayerGameState::ViewingHistory => {
                println!("[INFO] Ignoring clicks when viewing history");
            },
            TwoPlayerGameState::WaitingPieceSelection => {
                let cell = game_state.cells()[click_index];

                if !cell.is_empty() && cell.piece_color() == game_state.turn() {
                    let possible_moves = game_state.generate_possible_moves(click_index);
                    view_state.show_moves(&possible_moves);
                    view_state[click_index].set_select();

                    self.state = TwoPlayerGameState::PieceSelected { index: click_index, possible_moves };

                    ChessGame::instance().refresh_view();
                }
            },
            TwoPlayerGameState::PieceSelected { index, ref possible_moves } => {
                if click_index == index {
                    view_state.clear_moves(possible_moves);
                    view_state[click_index].remove_select();

                    self.state = TwoPlayerGameState::WaitingPieceSelection;
                } else if let Some(to_apply) = possible_moves.match_move_with_dest_index(click_index) {
                    view_state.clear_moves(possible_moves);
                    view_state[index].remove_select();

                    if let Some(last_move) = &mut self.last_move {
                        view_state.clear_move(last_move);
                    }

                    game_state.apply_move(&to_apply);

                    view_state.show_move(&to_apply);
                    self.last_move = Some(to_apply);

                    self.state = TwoPlayerGameState::WaitingPieceSelection;

                    self.history.commit_state(game_state.clone(), view_state.clone());

                }

                ChessGame::instance().refresh_view();
            }
        };
    }

    fn handle_key_press(&mut self, key: char, game_state: &mut GameState, view_state: &mut Board<ViewState>) {
        match key {
            'h' => {
                if let Some((g_s, g_v)) = self.history.undo() {
                    *game_state = g_s.clone();
                    *view_state = g_v.clone();

                    self.state = TwoPlayerGameState::ViewingHistory;
                    ChessGame::instance().refresh_view();
                }
            },
            'l' => {
                if let Some((g_s, g_v)) = self.history.redo() {
                    *game_state = g_s.clone();
                    *view_state = g_v.clone();

                    ChessGame::instance().refresh_view();
                }

                if self.history.is_at_latest() {
                    self.state = TwoPlayerGameState::WaitingPieceSelection;
                }
            },
            _ => {
                println!("[INFO] Ignoring key press: {}", key);
            }
        }
    }
}
