use native_dialog::FileDialog;

use crate::core::board::{Board, CellIndex};
use crate::view_model::ViewState;
use crate::core::game::GameState;
use crate::{ChessGame, Event};

use self::historical_matches::HistoricalMode;
use self::two_player::TwoPlayerMode;

mod two_player;
mod historical_matches;

trait GameMode: std::fmt::Debug + Send + Sync {
    fn init_mode(&mut self, game_state: &GameState, view_state: &Board<ViewState>);
    fn handle_click(&mut self, index: CellIndex, game_state: &mut GameState, view_state: &mut Board<ViewState>);
    fn handle_key_press(&mut self, key: char, game_state: &mut GameState, view_state: &mut Board<ViewState>);
    fn handle_event(&mut self, event: Event, game_state: &mut GameState, view_state: &mut Board<ViewState>);
}

#[derive(Debug)]
pub struct GameEngine {
    mode: Option<Box<dyn GameMode>>,
    game_state: GameState,
    view_state: Board<ViewState>,
}

impl GameEngine {

    pub fn new_game(game_state: GameState, view_state: Board<ViewState>) -> Self {
        Self {
            mode: None,
            game_state,
            view_state,
        }
    }

    fn set_mode<Mode: GameMode + 'static>(&mut self, mut mode: Mode) {
        mode.init_mode(&self.game_state, &self.view_state);
        self.mode = Some(Box::new(mode));
    }

    pub fn game_state(&self) -> &GameState {
        &self.game_state
    }

    pub fn view_state(&self) -> &Board<ViewState> {
        &self.view_state
    }

    pub fn handle_event(&mut self, event: Event) {
        match event {
            Event::InitEngine => {
                self.new_two_player_mode();
            }
            Event::Click(c) => {
                self.handle_click(c)
            },
            Event::KeyPress(c) => {
                self.handle_key_press(c)
            },
            e => {
                if let Some(mode) = self.mode.as_deref_mut() {
                    mode.handle_event(e, &mut self.game_state, &mut self.view_state);
                };
            }
        }
    }

    fn new_two_player_mode(&mut self) {
        self.set_mode(TwoPlayerMode::new());
    }

    fn handle_click(&mut self, cell_index: CellIndex) {
        if let Some(mode) = self.mode.as_deref_mut() {
            mode.handle_click(cell_index, &mut self.game_state, &mut self.view_state);
        };
    }

    fn handle_key_press(&mut self, key: char) {
        match key {
            'q' => {
                std::process::exit(0);
            },
            'n' => {
                self.game_state = GameState::default_starting_game_state();
                self.view_state = Board::<ViewState>::empty();
                self.new_two_player_mode();

                ChessGame::instance().refresh_view();
            },
            'o' => {
                if let Some(path) = FileDialog::new()
                    .set_location("./")
                    .add_filter("PGN files", &["pgn"])
                    .add_filter("All files", &["*"])
                    .show_open_single_file()
                    .ok()
                    .flatten() {

                    let historical_games_mode = HistoricalMode::from_file(&path);

                    self. game_state = GameState::default_starting_game_state();
                    self. view_state = Board::<ViewState>::empty();
                    self.set_mode(historical_games_mode);

                    ChessGame::instance().refresh_view();
                }
            },
            _ => {

                if let Some(mode) = self.mode.as_deref_mut() {
                    mode.handle_key_press(key, &mut self.game_state, &mut self.view_state);
                };
            }
        }
            //'\u{1b}' => {
            //    //match &mut self.mode {
            //    //    GameMode::None => { },
            //    //    GameMode::TwoPlayer(t) => {
            //    //        if let TwoPlayerGameState::ViewingHistory { .. } = &t.state {
            //    //            t.state = TwoPlayerGameState::WaitingPieceSelection;
            //    //            self.game_board = t.history.last().unwrap().clone();
            //    //        }
            //    //    },
            //    //    GameMode::Historical(s) => {
            //    //        let old_selected_data = std::mem::take(&mut s.selected_data);

            //    //        if let Some((sel, moves)) = old_selected_data {
            //    //            Self::clear_highlighted_possible_moves_for(&mut self.game_board, sel, &moves);
            //    //            s.selected_data = None;
            //    //        }

            //    //    },
            //    //}
            //},
    }
}

