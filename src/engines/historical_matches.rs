use std::path::Path;

use crate::core::board::{Board, CellIndex};
use crate::view_model::ViewState;
use crate::utils::pgn::{GameData, GameFile};
use crate::core::game::{GameState, PossibleMoves};
use crate::{ChessGame, Event};

use super::GameMode;


#[derive(Debug)]
pub struct HistoricalMode {
    games: Vec<GameData>,
    game_index: usize,

    selected_data: Option<(CellIndex, PossibleMoves)>
}

impl HistoricalMode {
    pub fn from_file(path: &Path) -> Self {
        GameFile::read_pgn_file(path, |g| {
            ChessGame::instance().send_event(Event::LoadedGame(Box::new(g)));
        });

        HistoricalMode {
            games: Vec::new(),
            game_index: 0,
            selected_data: None
        }
    }

    //pub fn is_empty(&self) -> bool {
    //    self.games.is_empty()
    //}
}

impl GameMode for HistoricalMode {
    fn init_mode(&mut self, _: &GameState, _: &Board<ViewState>) { }

    fn handle_event(&mut self, event: Event, game_state: &mut GameState, view_state: &mut Board<ViewState>) {
        match event {
            Event::LoadedGame(mut g) => {
                g.history.reset_index();
                self.games.push(*g);

                if self.game_index == 0 && self.games.len() == 1 {
                    // This is the first game. Show it immediately
                    let (g_s, g_v) = self.games[self.game_index].history.current();
                    *game_state = g_s.clone();
                    *view_state = g_v.clone();

                    ChessGame::instance().refresh_view();
                }
            },
            _ => {}
        }
    }

    fn handle_click(&mut self, click_index: CellIndex, game_state: &mut GameState, view_state: &mut Board<ViewState>) {
        let clicked_piece = game_state.cells()[click_index];

        if !clicked_piece.is_empty() && clicked_piece.piece_color() == game_state.turn() {
            if let Some((old_selected, old_moves)) = &mut self.selected_data {
                view_state.clear_moves(old_moves);
                view_state[*old_selected].remove_select();

                if *old_selected == click_index {
                    self.selected_data = None;

                    ChessGame::instance().refresh_view();
                    return;
                }
            }

            let possible_moves = game_state.generate_possible_moves(click_index);
            view_state.show_moves(&possible_moves);
            view_state[click_index].set_select();
            self.selected_data = Some((click_index, possible_moves));

            ChessGame::instance().refresh_view();
        }
    }

    fn handle_key_press(&mut self, key: char, game_state: &mut GameState, view_state: &mut Board<ViewState>) {
        match key {
            'h' => {
                if let Some((g_s, g_v)) = self.games[self.game_index].history.undo() {
                    *game_state = g_s.clone();
                    *view_state = g_v.clone();

                    ChessGame::instance().refresh_view();
                }
            },
            'l' => {
                if let Some((g_s, g_v)) = self.games[self.game_index].history.redo() {
                    *game_state = g_s.clone();
                    *view_state = g_v.clone();

                    ChessGame::instance().refresh_view();
                }
            },
            'k' => {
                if self.game_index > 0 {
                    self.game_index -= 1;

                    let (g_s, g_v) = self.games[self.game_index].history.current();
                    println!("[DEBUG] Showing game {}", self.game_index);

                    *game_state = g_s.clone();
                    *view_state = g_v.clone();

                    ChessGame::instance().refresh_view();
                }
            },
            'j' => {
                if self.game_index < self.games.len() - 1 {
                    self.game_index += 1;

                    let (g_s, g_v) = self.games[self.game_index].history.current();
                    println!("[DEBUG] Showing game {}", self.game_index);

                    *game_state = g_s.clone();
                    *view_state = g_v.clone();

                    ChessGame::instance().refresh_view();
                }
            },
            _ => {
                println!("[INFO] Ignoring key press: {}", key);
            }
        }
    }
}
