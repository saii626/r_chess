use core::board::{self, Board, CellIndex, CellIndexIter};
use core::game::GameState;
use std::collections::VecDeque;
use std::sync::{Arc, Condvar, Mutex, OnceLock};
use std::rc::Rc;
use std::time::Duration;

use slint::{Model, ModelRc, VecModel, Weak};
use utils::pgn::GameData;
use view_model::ViewState;

use crate::engines::GameEngine;

slint::include_modules!();

mod core;
mod engines;
mod view_model;
mod utils;

pub enum Event {
    InitEngine,

    Click(CellIndex),
    KeyPress(char),

    LoadedGame(Box<GameData>),

    RefreshBoard,
}

impl std::fmt::Debug for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InitEngine => write!(f, "InitEngine"),
            Self::Click(arg0) => f.debug_tuple("Click").field(arg0).finish(),
            Self::KeyPress(arg0) => f.debug_tuple("KeyPress").field(arg0).finish(),
            Self::LoadedGame(_) => f.debug_tuple("LoadedGame").field(&"...").finish(),
            Self::RefreshBoard => write!(f, "RefreshBoard"),
        }
    }
}

static mut GAME: OnceLock<ChessGame> = OnceLock::new();


#[derive(Clone)]
pub struct ChessGame {
    // The main sync mechanism between UI thread and background thread. Just a FIFO queue with
    // events to process
    events_to_process: Arc<(Mutex<VecDeque<Event>>, Condvar)>,

    // Reference to slint view.
    // MAKE SURE TO UPDATE THIS INSIDE SLINT EVENT LOOP THREAD
    ui: Weak<BoardView>,
}

impl std::fmt::Debug for ChessGame {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ChessGame")
            .field("events_to_process", &self.events_to_process)
            .finish()
    }
}

impl ChessGame {
    pub fn instance() -> ChessGame {
        unsafe { GAME.get().unwrap().clone() }
    }

    pub fn refresh_view(&self) {
        self.send_event(Event::RefreshBoard);
    }

    fn new_game(ui: Weak<BoardView>) -> Self {

        let chess_game = ChessGame {
            events_to_process: Arc::new((Mutex::new(VecDeque::new()), Condvar::new())),
            ui: ui.clone(),
        };

        // Setup on click listener
        ui.upgrade().unwrap().on_clicked({
            let game = chess_game.clone();

            move |cell_index| {
                let cell_index = CellIndex::from_raw(cell_index.row.try_into().unwrap(), cell_index.col.try_into().unwrap());
                game.send_event(Event::Click(cell_index));
            }
        });

        // Setup key press listener
        ui.upgrade().unwrap().on_pressed({
            let game = chess_game.clone();

            move |key| {
                let ch = {
                    let mut chars = key.as_str().chars();
                    chars.next().unwrap()
                };

                game.send_event(Event::KeyPress(ch));
            }
        });

        chess_game
    }

    fn run(&self) -> Result<(), slint::PlatformError> {
        std::thread::spawn({
            let game = self.clone();

            move || { game.process_events() }
        });

        self.ui.upgrade().unwrap().run()?;

        Ok(())
    }

    pub fn send_event(&self, event: Event) {
        let mut lock = self.events_to_process.0.lock().unwrap();
        lock.push_back(event);
        self.events_to_process.1.notify_all();
    }

    fn process_events(&self) {
        let game_state = GameState::default_starting_game_state();
        let view_state = Board::<ViewState>::empty();

        let cells = game_state.cells().clone();
        let mut engine = GameEngine::new_game(game_state, view_state.clone());

        let mut view_model = view_model::BoardView::new(cells, Board::<board::Color>::default(), view_state);

        // Initialize the board view model
        {
            let models: Vec<_> = view_model.iter_models().collect();

            slint::invoke_from_event_loop({
                let ui_ref = self.ui.clone();

                move || {
                    let ui = ui_ref.upgrade().unwrap();

                    let board_model: VecModel<ModelRc<_>> = VecModel::default();

                    let mut row_model = VecModel::default();
                    for (_, model) in models {
                        row_model.push(model);

                        if row_model.row_count() == 8 {
                            board_model.push(Rc::new(row_model).into());
                            row_model = VecModel::default();
                        }
                    }


                    ui.set_board(BoardModel { cells: Rc::new(board_model).into() });
                }
            }).expect("Unable to setup board from background thread");
        }

        self.send_event(Event::InitEngine);

        loop {
            let event = {
                let mut lock = self.events_to_process.0.lock().unwrap();
                loop {
                    match lock.pop_front() {
                        Some(e) => break Some(e),
                        None => {
                            let (l, res) = self.events_to_process.1.wait_timeout(lock, Duration::from_millis(30)).unwrap();
                            lock = l;

                            if res.timed_out() {
                                break None;
                            }
                        },
                    }
                }
            };

            macro_rules! update_ui {
                ($lvl:literal) => {
                    {
                        let mut updates = Vec::new();

                        for (index, cell_model) in (GameUpdateIter { game: &engine, view_model: &mut view_model, index: CellIndexIter::new() }) {
                            updates.push((index, cell_model));
                        }

                        if updates.is_empty() {
                            continue;
                        }

                        slint::invoke_from_event_loop({
                            let ui_ref = self.ui.clone();
                            println!("[{}] Updating {} cells", $lvl, updates.len());

                            move || {
                                let ui = ui_ref.upgrade().unwrap();

                                for (index, model) in updates {
                                    let (row, col) = index.to_raw();
                                    ui.get_board().cells.row_data(row as usize).unwrap().set_row_data(col as usize, model);
                                }
                            }
                        }).expect("Unable to update board from background thread");
                    }
                }
            }

            if let Some(event) = event {
                if let Event::RefreshBoard = event {
                    update_ui!("DEBUG")
                } else {
                    println!("[DEBUG] Event {event:?}");
                    engine.handle_event(event);
                }
            } else {
                update_ui!("INFO")
            }
        }
    }
}

struct GameUpdateIter<'a> {
    game: &'a GameEngine,
    view_model: &'a mut view_model::BoardView,
    index: CellIndexIter,
}

impl<'a> Iterator for GameUpdateIter<'a> {
    type Item = (CellIndex, CellModel);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(index) = self.index.next() {
            let new_cell_game_state = self.game.game_state().cells()[index];
            let new_cell_view_state = self.game.view_state()[index];

            let mut is_updated = false;

            if new_cell_game_state != self.view_model.cells()[index] {
                self.view_model.cells_mut()[index] = new_cell_game_state;
                is_updated = true;
            }

            if new_cell_view_state != self.view_model.states()[index] {
                self.view_model.states_mut()[index] = new_cell_view_state;
                is_updated = true;
            }

            if is_updated {
                Some((index, self.view_model.get_model_at_index(index)))
            } else {
              self.next()
            }
        } else { None }
    }
}

fn main() -> Result<(), slint::PlatformError> {

    let view_strong_ref = BoardView::new()?;

    let chess_game = ChessGame::new_game(view_strong_ref.as_weak());
    unsafe { GAME.set(chess_game.clone()).unwrap(); }

    chess_game.run()
}


