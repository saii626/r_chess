use std::str::FromStr;
use std::sync::OnceLock;
use std::time::SystemTime;
use std::{collections::HashMap, path::Path};

use regex::Regex;

use crate::board::{Board, Color};
use crate::core::board::CellIndexIter;
use crate::core::game::GameState;
use crate::view_model::ViewState;

use std::fmt;
use std::iter::Peekable;
use std::str::Chars;

use super::history::HistoryKeeper;

#[derive(Debug, PartialEq, Default, Clone)]
enum Token {
    #[default]
    Empty,

    // Single character tokens
    TagOpen, // [
    TagClose,  // ]

    Tag (String),
    TagValue (String),

    WhiteMoveIndex (u64),
    BlackMoveIndex (u64),

    SanMove (String),

    GameEnd (String),
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Token::Empty => Ok(()),
            Token::TagOpen => write!(f, "["),
            Token::TagClose => write!(f, "]"),
            Token::Tag(t) => write!(f, "{t}"),
            Token::TagValue(v) => write!(f, "\"{v}\""),
            Token::WhiteMoveIndex(i) => write!(f, "{i}. "),
            Token::BlackMoveIndex(i) => write!(f, "{i}... "),
            Token::SanMove(m) => write!(f, "{m}"),
            Token::GameEnd(m) => write!(f, "{m}"),
        }
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
struct TokenData {
    pub token: Token,
    pub line: u64,
    pub col: u64,
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum ParsingState {
    Unknown,
    Tag,
    Movetext,
}

struct Lexer<'a> {
    source: Peekable<Chars<'a>>,

    state: ParsingState,
    line: u64,
    column: u64,
}

impl<'a> Iterator for Lexer<'a> {
    type Item = TokenData;

    fn next(&mut self) -> Option<Self::Item> {
        self.scan_next_token()
    }
}

impl<'a> Lexer<'a> {

    fn new(source: &'a str) -> Self {
        Lexer {
            source: source.chars().peekable(),
            line: 1,
            state: ParsingState::Unknown,
            column: 0,
        }
    }

    fn advance(&mut self) -> Option<char> {
        self.column += 1;
        self.source.next()
    }

    fn line_change(&mut self) {
        self.line += 1;
        self.column = 0;
    }

    fn consume_till_char(&mut self, expected: char) -> String {
        let mut str = String::new();

        loop {
            match self.advance() {
                Some(ch) => {
                    if ch == '\n' { self.line_change(); }
                    if ch == expected { break; }
                    str.push(ch);
                },
                None => {
                    panic!("Unexpected end of file reached when consuming till `{expected}` char");
                }
            }
        }

        str
    }

    fn scan_next_token(&mut self) -> Option<TokenData> {

        Some(match self.source.peek()? {
            ' ' | '\r' | '\t' => {
                self.advance();
                self.scan_next_token()?
            },
            '\n' => {
                self.advance();
                self.line_change();
                self.scan_next_token()?
            },
            '[' => {
                assert!(self.state == ParsingState::Unknown);
                self.advance();
                self.state = ParsingState::Tag;
                TokenData { token: Token::TagOpen, line: self.line, col: self.column }
            },
            ']' => {
                assert!(self.state == ParsingState::Tag);
                self.advance();
                self.state = ParsingState::Unknown;
                TokenData { token: Token::TagClose, line: self.line, col: self.column }
            },
            '"' => {
                assert!(self.state == ParsingState::Tag);
                self.advance();
                let str = self.consume_till_char('"');
                TokenData { token: Token::TagValue (str), line: self.line, col: self.column }
            },
            ';' => {
                assert!(self.state == ParsingState::Movetext);
                self.consume_till_char('\n');
                self.scan_next_token()?
            }
            '{' => {
                assert!(self.state == ParsingState::Movetext);
                self.consume_till_char('}');
                self.scan_next_token()?
            },
            _ => {
                match self.state {
                    ParsingState::Unknown => {
                        self.state = ParsingState::Movetext;
                        self.parse_move_text()?
                    },
                    ParsingState::Tag => {
                        let mut str = String::new();

                        loop {
                            match self.source.peek().cloned() {
                                Some(ch) => {
                                    if ch == '\n' {
                                        self.advance();
                                        self.line_change();
                                        continue;
                                    }
                                    if ch == '"' { break; }
                                    self.advance();
                                    str.push(ch);
                                },
                                None => {
                                    panic!("Unexpected end of file reached when consuming tag at {}:{}", self.line, self.column);
                                }
                            }
                        }

                        TokenData { token: Token::Tag(str.as_str().trim().to_string()), line: self.line, col: self.column }
                    },
                    ParsingState::Movetext => {
                        self.parse_move_text()?
                    },
                }
            },
        })
    }

    fn parse_move_text(&mut self) -> Option<TokenData> {
        let mut str = String::new();

        let game_end_matcher = {
            static GAME_END_RE: OnceLock<Regex> = OnceLock::new();
            GAME_END_RE.get_or_init(|| Regex::new(r"\d-\d").unwrap())
        };

        let move_number_matcher = {
            static GAME_WHITE_MOVE_NUMBER_RE: OnceLock<Regex> = OnceLock::new();
            GAME_WHITE_MOVE_NUMBER_RE.get_or_init(|| Regex::new(r"^(\d*)(\.*)$").unwrap())
        };

        //let black_move_number_matcher = {
        //    static GAME_BLACK_MOVE_NUMBER_RE: OnceLock<Regex> = OnceLock::new();
        //    GAME_BLACK_MOVE_NUMBER_RE.get_or_init(|| Regex::new(r"^(\d*)\.{3}$").unwrap())
        //};

        loop {
            match self.source.peek().cloned() {
                Some(ch) => {
                    if ch == '.' {
                        self.advance();
                        str.push(ch);

                        while let Some(ch) = self.source.peek().cloned() {
                            if ch == '.' {
                                self.advance();
                                str.push(ch);
                            } else { break; }
                        }

                        break;
                    }

                    if ch == ' ' {
                        self.advance();
                        break;
                    }

                    if ch == '\n' {
                        self.advance();
                        self.line_change();
                        break;
                    }
                    self.advance();
                    str.push(ch);
                },
                None => {
                    panic!("Unexpected end of file reached when consuming tag at {}:{}", self.line, self.column);
                }
            }
        }

        let trimmed_content = str.as_str().trim();
        let token = match move_number_matcher.captures(trimmed_content) {
            Some(n) => {
                match u64::from_str(n.get(1).unwrap().as_str()) {
                    Ok(u) => {
                        let cap_2 = n.get(2).unwrap().as_str();

                        if cap_2 == "." {
                            Token::WhiteMoveIndex(u)
                        } else if cap_2 == "..."  {
                            Token::BlackMoveIndex(u)
                        } else {
                            panic!("Unable to parse move number")
                        }
                    },
                    Err(e) => {
                        println!("[ERROR] Unable to parse string \"{}\" to integer due to {}", str, e);
                        return None;
                    },
                }
            },
            None => {
                if game_end_matcher.is_match(trimmed_content) || trimmed_content == "*" {
                    self.state = ParsingState::Unknown;
                    Token::GameEnd(trimmed_content.to_string())
                } else {
                    Token::SanMove(trimmed_content.to_string())
                }
            }
        };

        Some(TokenData { token, line: self.line, col: self.column })
    }
}

#[derive(Debug)]
pub struct GameData {
    pub tags: HashMap<String, String>,
    pub history: HistoryKeeper,
}

struct Parser<'a> {
    lexer: Lexer<'a>,
}

impl<'a> Iterator for Parser<'a> {
    type Item = GameData;

    fn next(&mut self) -> Option<Self::Item> {
        let mut tags = HashMap::new();

        let mut game_state = GameState::default_starting_game_state();
        let mut view_state = Board::<ViewState>::empty();

        let mut history = HistoryKeeper::new();
        history.commit_state(game_state.clone(), view_state.clone());

        while let Some(tok) = self.lexer.next() {
            match tok.token {
                Token::Empty => { return None; },
                Token::TagOpen => {},
                Token::TagClose => {},
                Token::Tag(tag) => {
                    let val_token = self.lexer.next().expect("[ERROR] Tag value not provided");
                    let val = match val_token.token {
                        Token::TagValue(v) => v,
                        _ => unreachable!(),
                    };

                    tags.insert(tag, val);
                },
                Token::TagValue(_) => unreachable!(),
                Token::WhiteMoveIndex(_) => {
                    assert!(game_state.turn() == Color::White);
                },
                Token::BlackMoveIndex(_) => {
                    assert!(game_state.turn() == Color::Black);
                },
                Token::SanMove(m) => {
                    view_state = Board::<ViewState>::empty();
                    let current_cells = game_state.cells().clone();
                    //println!("{}:{} move {}, board: {}", tok.line, tok.col, m, game_state.cells());
                    game_state.apply_san_move(m.as_str());

                    for index in CellIndexIter::new() {
                        if current_cells[index] != game_state.cells()[index] {
                            view_state[index].set_update();
                        }
                    }
                    history.commit_state(game_state.clone(), view_state.clone());
                },
                Token::GameEnd(_) => {
                    return Some(GameData { tags, history });
                },
            }
        }

        None
    }
}

#[derive(Debug)]
pub struct GameFile {
    //pub games: Vec<GameData>,
}

impl GameFile {
    pub fn read_pgn_file<F: FnMut(GameData) + Send + 'static>(path: &Path, mut cb: F) {
        println!("[INFO] loading file {path:?}");
        let path = path.to_path_buf();

        std::thread::spawn(move || {
            let start = SystemTime::now();
            let path = path.as_path();

            let file_contents = std::fs::read(path).unwrap_or_else(|_| panic!("Unable to read file {path:?}"));

            let decoded_source = match String::from_utf8(file_contents) {
                Ok(s) => s,
                Err(e) => panic!("Unable to decode file as utf8. {e}"),
            };

            let read_time = SystemTime::now().duration_since(start).unwrap();

            let lexer = Lexer::new(&decoded_source);
            let parser = Parser { lexer };

            let mut count = 0;
            for tok in parser {
                cb(tok);
                count += 1;
            }

            let parsed_time = SystemTime::now().duration_since(start).unwrap();
            println!("[INFO] Lodaed {} games. Read time: {:?}, Parse time: {:?}", count, read_time, parsed_time);
        });
    }
}

