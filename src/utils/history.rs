use crate::core::board::Board;
use crate::core::game::GameState;
use crate::view_model::ViewState;


#[derive(Debug, Clone, Default)]
pub struct HistoryKeeper {
    game_state_history: Vec<GameState>,
    game_view_history: Vec<Board<ViewState>>,
    index: usize,
}

impl HistoryKeeper {
    pub fn new() -> Self {
        Self { game_state_history: vec![], game_view_history: vec![], index: 0 }
    }

    pub fn commit_state(&mut self, game: GameState, state: Board<ViewState>) {
        assert!(self.index == self.game_state_history.len());
        assert!(self.index == self.game_view_history.len());

        self.game_state_history.push(game);
        self.game_view_history.push(state);
        self.index = self.game_state_history.len();
    }

    pub fn is_at_latest(&self) -> bool {
        self.index == self.game_state_history.len()
    }

    pub fn reset_index(&mut self) {
        assert!(self.game_state_history.len() > 1);
        self.index = 1;
    }

    pub fn undo(&mut self) -> Option<(&GameState, &Board<ViewState>)> {
        if self.index > 1 {
            let index = self.index - 2;
            self.index -= 1;
            Some((&self.game_state_history[index], &self.game_view_history[index]))
        } else if !self.game_state_history.is_empty() { 
            Some((&self.game_state_history[0], &self.game_view_history[0]))
        } else {
            None
        }
    }

    pub fn redo(&mut self) -> Option<(&GameState, &Board<ViewState>)> {
        if self.index < self.game_state_history.len() {
            let index = self.index;
            self.index += 1;
            Some((&self.game_state_history[index], &self.game_view_history[index]))
        } else if !self.game_state_history.is_empty() { 
            Some((&self.game_state_history[self.game_state_history.len() - 1], &self.game_view_history[self.game_view_history.len() - 1]))
        } else {
            None
        }
    }

    pub fn current(&self) -> (&GameState, &Board<ViewState>) {
        assert!(self.index > 0);
        let index = self.index - 1;
        (&self.game_state_history[index], &self.game_view_history[index])
    }
}

